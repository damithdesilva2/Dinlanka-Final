import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import AdminNavbar from "../layouts/AdminNavbar";
import {
  Card,
  Button,
  CardImg,
  CardTitle,
  CardText,
  CardColumns,
  CardSubtitle,
  CardBody,
  Row,
  Col,
  CardHeader,
  Container
} from "reactstrap";
import firebase from "../../config/fbConfig.js";
import {Bar, Pie,Line }from 'react-chartjs-2';
var Chart = require("chart.js");

class AdminHome extends Component {
  chartRef1 = React.createRef();
  chartRef2 = React.createRef();
 
  constructor(props) {

    super(props);
    this.state ={
      count1 :0,
      count2 :0,
      count3 :0,
      co1:0,
      co2:0,
      co3:0,
      co4:0,
      co5:0
      //boards: []
    }
  }

   

  componentDidMount = () =>{
    this.userCount();
    this.shipmentCount();
    this.quatationCount();
    this.typeCount();
  
    
  
    
  }

  userCount = () => {
    var count1 = this.state.count1;
    var that = this
    const docref = firebase.firestore().collection('users');
    
    docref.get().then(snapshot => {
      snapshot.forEach(doc=> {
        if(doc.data().isAdmin =="no"){
          this.setState((state)=>({
            count1:that.state.count1 + 1
          })) 
        }
             
      })  
        
    })    
  }
  
//   userCountN = () => { 
//     var boards=[];
//     const docref = firebase.firestore().collection('users');   
//     docref.get().then(snapshot => {
//       snapshot.forEach(doc=> {
//         if(doc.data().isAdmin =="no"){
//         boards.push( doc.data().username);
//     }
//     console.log(boards);
//     var q1 = boards.length;
//     return q1;
//     //this.z1 = boards.length;
//   //   this.setState({
//   //     boards
//   //  });
//   });
    
    
//     });
// }
typeCount = () => {
  var co1 = this.state.co1;
  var co2 = this.state.co2;
  var co3 = this.state.co3;
  var co4 = this.state.co4;
  var co3 = this.state.co5;
  var that = this
  const docref = firebase.firestore().collection('shipments');
  
  docref.get().then(snapshot => {
    snapshot.forEach(doc=> {
      if(doc.data().type =="Electronics-(No Battery)"){
        this.setState((state)=>({
          co1:that.state.co1 + 1
        })) 
      }else if(doc.data().type =="Books and Collectibles")
      {
        this.setState((state)=>({
          co2:that.state.co2 + 1
        })) 
      }else if(doc.data().type =="Dry Food and Supplements"){
        this.setState((state)=>({
          co3:that.state.co3 + 1
        })) 
      }else if(doc.data().type =="Fashion, Health, Beauty"){
        this.setState((state)=>({
          co4:that.state.co4 + 1
        }))  
      }else{
        this.setState((state)=>({
          co5:that.state.co5 + 1
        }))
      }
    })
  const myChartRef1 = this.chartRef1.current.getContext("2d");
  new Chart(myChartRef1, {
    type: "bar",
    data: {
      labels: ["Electronics-(No Battery)", "Books and Collectibles", "Dry Food and Supplements","Fashion, Health, Beauty","Toy, Sports, Leisure"],
      datasets: [
        {
          label: "Shipments Types",
          data: [this.state.co1,this.state.co2,this.state.co3,this.state.co4,this.state.co5],
          
          backgroundColor: [
            "rgba(232, 195, 61, 0.2)",
            "rgba(255, 99, 132, 0.2)",
            "rgba(7, 139, 61, 0.2)",
            "rgba(195, 56, 174, 0.2)",
            "rgba(255, 168, 0, 0.2)"
          ]
        }
      ]
    }
  });
  const myChartRef2 = this.chartRef2.current.getContext("2d");
  new Chart(myChartRef2, {
    type: "pie",
    data: {
      labels: ["Electronics-(No Battery)", "Books and Collectibles", "Dry Food and Supplements","Fashion, Health, Beauty","Toy, Sports, Leisure"],
      datasets: [
        {
          label: "Shipments Types",
          data: [this.state.co1,this.state.co2,this.state.co3,this.state.co4,this.state.co5],
          
          backgroundColor: [
            "rgba(232, 195, 61, 0.2)",
            "rgba(255, 99, 132, 0.2)",
            "rgba(7, 139, 61, 0.2)",
            "rgba(195, 56, 174, 0.2)",
            "rgba(255, 168, 0, 0.2)"
          ]
        }
      ]
    }
  });        
      
  })
  
}
  shipmentCount = () => {
    var count2 = this.state.count2;
    var that = this
    const docref = firebase.firestore().collection('shipments');
    
    docref.get().then(snapshot => {
      snapshot.forEach(doc=> {
          that.setState({
            count2 :that.state.count2 + 1
          })               
      })  
    })   
  }

  quatationCount = () => {
    var count3 = this.state.count3;
    var that = this
    const docref = firebase.firestore().collection('comments');
    
    docref.get().then(snapshot => {
      snapshot.forEach(doc=> {
          that.setState({
            count3 :that.state.count3 + 1
          })               
      })  
        
    })   
  }
  render() {
    
    const { projects, auth } = this.props;
    if (!auth.uid) return <Redirect to="/signin" />;
    else console.log(auth.uid);

    return (
      <React.Fragment>
        
        <AdminNavbar />
        <div className="dashboard container">
          <Container>
            {""}
            {""}
            <div className="row" style={{marginTop:'40px',marginBottom:'40px'}}>
  <div className="column" style={{float: 'left',width: '33.33%'}}>
  <Card>
              <CardHeader className="bg-primary">
                <strong> <h5>
                Agent count
                </h5></strong>
              </CardHeader>
              <CardBody>
                <Card>
                  <CardBody>
                    <CardTitle></CardTitle>
                    <CardSubtitle><h4> &nbsp; &nbsp; {this.state.count1}  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<i className="fa fa-user icon"></i></h4></CardSubtitle>                   
                    <CardText />
                  </CardBody>
                </Card>
              </CardBody>
            </Card>
  </div>
  <div className="column" style={{float: 'left',width: '33.33%'}}>
  <Card>
              <CardHeader className="bg-primary">
                <strong> <h5>
                Shipment count
                </h5></strong>
              </CardHeader>
              <CardBody>
                <Card>
                  <CardBody>
                    <CardTitle></CardTitle>
                    <CardSubtitle> <h4> &nbsp; &nbsp; {this.state.count2}  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<i className="fas fa-dolly-flatbed"></i></h4></CardSubtitle>
                    <CardText />
                  </CardBody>
                </Card>
              </CardBody>
            </Card>
  </div>
  <div className="column" style={{float: 'left',width: '33.33%'}}>
  <Card>
              <CardHeader className="bg-primary">
                <strong> <h5>
                Messages count
                </h5></strong>
              </CardHeader>
              <CardBody>
                <Card>
                  <CardBody>
                    <CardTitle></CardTitle>
                    <CardSubtitle> <h4>  &nbsp; &nbsp; {this.state.count3}  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<i className="fas fa-copy"></i> </h4></CardSubtitle>
                    <CardText />
                  </CardBody>
                </Card>
              </CardBody>
            </Card>
  </div>
</div>
<div className="row">
  <div className="column" style={{float: 'left',width: '50%'}}>
  <Card>
              <CardHeader className="bg-primary">
                <strong> <h5>
                  Shipment Statistics Bar Chart
                  </h5></strong>
              </CardHeader>
              <CardBody>
                <Card>
                  <CardBody>
                    <CardTitle></CardTitle>
                    <canvas
                    id="myChart"
                    ref={this.chartRef1}
                    />
                  </CardBody>
                </Card>
              </CardBody>
            </Card>
  </div>
  <div className="column" style={{float: 'left',width: '50%'}}>
  <Card>
              <CardHeader className="bg-primary">
                <strong> <h5>
                Shipment Statistics Pie Chart
                </h5></strong>
              </CardHeader>
              <CardBody>
                <Card>
                  <CardBody>
                    <CardTitle></CardTitle>
                    <canvas
                    id="myChart"
                    ref={this.chartRef2}
                    />
                  </CardBody>
                </Card>
              </CardBody>
            </Card>
  </div>
</div>     
            
            
            

          </Container>
        </div>
      </React.Fragment>
    );
  }
}

const mapStateToProps = state => {
  console.log(state);
  return {
    auth: state.firebase.auth,
    profile: state.firebase.profile
  };
};

export default connect(mapStateToProps)(AdminHome);
