import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { signUp } from '../../store/actions/authActions'
import { Button, Card, CardBody, CardGroup, Col,CustomInput,FormGroup, Container,Modal,ModalHeader,ModalBody,ModalFooter, Form, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';
import { Label, FormFeedback, FormText , CardFooter,CardHeader,Alert} from 'reactstrap';
import AdminNavbar from '../layouts/AdminNavbar';
import { withRouter } from "react-router";


class SignUp extends Component {
  state = {
    email: '',
    password: '',
    fullName: '',
    username: '',
    working:'',
    country:'',
    isAdmin:''
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    })
  }


//   handleError = () => {
//     if (this.state.fullname == '' || this.state.username == '' || this.state.email== '' || this.state.password == ''){
//       this.setState({
//         user_er:false
//       })
//     }
//     else {
//       this.setState({
//         user_er:true
//       })
//   }
// }
handleSubmit = (e) => {
  e.preventDefault();
  this.props.signUp(this.state);

  setTimeout(
    function() {
      if(!this.props.authError){
        console.log("ghghg"+this.props.authError)
        this.props.history.push('/manage');
         }
    }
    .bind(this),
    2000
);

}
  render() {
    const { auth,authError, profile } = this.props;
    if(!auth.uid && !profile.isAdmin){return <Redirect to='/signin' />}

    return (

      <React.Fragment>
          <AdminNavbar/>
          <div className="container">

          <Card style={{width:'800px',marginLeft:'150px', marginTop:'50px'}}>
              <CardHeader className='bg-primary'>
                <strong><h3>Regsiter Agents</h3></strong>
              </CardHeader>
              <CardBody>
              { authError ?
              <Alert color="danger">  <p>{authError}</p> </Alert>
              : null }
                <Form id="tex" onSubmit={this.handleSubmit} className="form-horizontal">
                  <FormGroup row>
                    <Col md="3">

                    </Col>
                    <Col xs="12" md="9">
                      <p className="form-control-static"></p>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="text-input" >Full Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id='fullname' onChange={this.handleChange} name="text-input" placeholder="Full name" />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="text-input">User Name</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="username" name="text-input" placeholder="username" onChange={this.handleChange} />
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                  <Col md="2">
                      <Label htmlFor="text-input">User type</Label>
                    </Col>
                    <Col xs="12" md="9">
                    <Input type="select" name="isAdmin" id="isAdmin" onChange={this.handleChange} >
                      <option value="no">Agent</option>
                      <option value="yes">Admin</option>
                      </Input>
                      </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="text-input">Country</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="select" id="country" name="country" placeholder="Country" onChange={this.handleChange}>
                      <option value="Sri Lanka">Sri Lanka</option>
                      <option value="United Kingdom">United Kingdom</option>
                      <option value="Singapore">Singapore</option>
                      </Input>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="text-input">Working place</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="working" name="text-input" placeholder="Working Place" onChange={this.handleChange} />

                    </Col>
                  </FormGroup>

                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="email-input">Email</Label>
                    </Col>
                    <Col xs="12" md="9">
                      <Input type="text" id="email" name="text-input" placeholder="email" onChange={this.handleChange} />

                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Col md="2">
                      <Label htmlFor="text-input">Password</Label>
                    </Col>
                    <Col xs="12" md="9" style={{width:'100px' }}>
                      <Input type="password" id="password" name="text-input" placeholder="password" onChange={this.handleChange} />
                    </Col>
                  </FormGroup>


              <CardFooter>
                <Button form="tex" type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
                <div class="divider" style={{width:'30px',height:'auto',display:'inline-block'}}/>
                <Button form="tex" type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
              </CardFooter>
              </Form>
              </CardBody>

           </Card>

      </div>
      </React.Fragment>
    );
  }
}


const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError,
    profile:state.firebase.profile,

  }
}

const mapDispatchToProps = (dispatch)=> {
  return {
    signUp: (creds) => dispatch(signUp(creds))
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(SignUp))
