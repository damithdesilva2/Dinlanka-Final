import React, { Component } from 'react';
import { Redirect } from 'react-router-dom';
import { Button, Card, CardBody, CardGroup, Col, Container, Form,  InputGroup,  Row ,Alert} from 'reactstrap';
import { signIn } from '../../store/actions/authActions'
import { connect } from 'react-redux'
import {TextInput,NavItem,CardPanel}from 'react-materialize';
import { reactReduxFirebase } from 'react-redux-firebase';
import './login.css';
import Background from '../../images/cover.jpg'
import logo from '../../assets/logo-edited.jpg'

class Login extends Component {
  state = {
    email : "",
    password : ""

  }
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.signIn(this.state)
  }
  handleChange = (e) => {
    this.setState({
      [e.target.id]:e.target.value
    })
  }
  render() {
    const { authError, auth, profile } = this.props;
    console.log(profile)
    if(auth.uid){
      if(profile.isAdmin) {return <Redirect to='/adminhome' />}
      else {return <Redirect to='/agenthome' />}
  
    }     
    return (
      <React.Fragment>
              <div className="bg-img" style={{backgroundImage: "url(" + Background + ")",minHeight: '100px'}}>
        <div className="row"> 
        <div className="col-md-6">
        <h1 className="mb-0"><a href="/" className="text-white h2 mb-0"><img src={logo} style={{marginTop:'25px', marginLeft:'50px'}}/></a></h1>
          </div>
  <div className="col-md-6 text-right">
  <a className="btn btn-primary " style={{marginTop:'25px', marginRight:'10px'}} href="/" role="button">Go Back To Home</a>
     
      
   
  </div>
  </div>
</div>
        <div className = 'd-flex justify-content-center' style={{paddingTop:'20px'}}><h1 style = {{margin:'auto'}}>Dinlanka Agent Login</h1></div>
      <div className="container h-100" style={{marginTop:'170px'}}>
      { authError ?
              <Alert color="danger">  <p>{authError}</p> </Alert>
              : null }
      <div className="d-flex justify-content-center h-100">
        <div className="user_card">
          <div className="d-flex justify-content-center">
            <div className="brand_logo_container">
              <img src={require('../../images/login.jfif')} className="brand_logo" alt="Logo" />
            </div>
          </div>
          <div className="d-flex justify-content-center form_container">         
            <form onSubmit={this.handleSubmit}>
              <div className="input-group mb-3">
                <div className="input-group-append">
                  <span className="input-group-text"><i className="fa fa-user icon"></i></span>
                </div>
                <input type="email" placeholder="Email" id='email'  onChange={this.handleChange} name className="form-control input_user"  />
              </div>
              <div className="input-group mb-2">
                <div className="input-group-append">
                  <span className="input-group-text"><i className="fas fa-key" /></span>
                </div>
                <input type="password" placeholder="Password" id='password'  onChange={this.handleChange} name className="form-control input_pass"  />
              </div>
              <div className="form-group">
                <div className="custom-control custom-checkbox">
                  <input type="checkbox" className="custom-control-input" id="customControlInline" />
                  <label className="custom-control-label" htmlFor="customControlInline">Remember me</label>
                </div>
              </div>
              <div className="d-flex justify-content-center mt-3 login_container">
            <button type="submit" name="button" className="btn login_btn">Login</button>
          </div>
            </form>
          </div>
          
          <div className="mt-4">
           
            <div className="d-flex justify-content-center links">
              <a href="#" >Forgot your password?</a>
            </div>
          </div>
        </div>
      </div>
    </div>
      <div className="app flex-row align-items-center" style={{marginTop:'70px'}}>      
    </div>
    </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
console.log(state)
  return{
    authError: state.auth.authError,
    profile:state.firebase.profile,
    auth: state.firebase.auth
    
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signIn: (creds) => dispatch(signIn(creds))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Login)
