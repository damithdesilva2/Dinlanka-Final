import React, { Component } from 'react';
import firebase from '../../config/fbConfig.js'
import { Link } from 'react-router-dom';
import AdminNavbar from '../layouts/AdminNavbar';
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import {  Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';
import {
    Button,
    Card,
    CardBody,
    Col,
    FormGroup,
    Form,
    Input
  } from "reactstrap";
  import { Label, CardFooter, CardHeader } from "reactstrap";
  

class Reply extends Component {

  constructor(props) {
    super(props);
    this.state = {
      board: {},
      key: '',
      modal: false
    };
    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  onChange = (e) => {
    const state = this.state
    state[e.target.id] = e.target.value;
    this.setState({board:state});
  }

  
  componentDidMount() {
    const ref = firebase.firestore().collection('comments').doc(this.props.match.params.id);
    ref.get().then((doc) => {
      if (doc.exists) {
        this.setState({
          board: doc.data(),
          key: doc.id,
          isLoading: false
        });
      } else {
        console.log("No such document!");
      }
    });
  }

  render() {
    const { auth,authError, profile } = this.props;
    if(!auth.uid && !profile.isAdmin){return <Redirect to='/signin' />}
    return (
        <React.Fragment>
      <AdminNavbar/>
      <div className="container">
          <Card>
            <CardHeader className="bg-primary">
              <strong>
                <h3>Reply Messages</h3>
              </strong>
            </CardHeader>
            <CardBody>
              <Form
                id="tex"
                className="form-horizontal"
              >
                <FormGroup row>
                  <Col md="3" />
                  <Col xs="12" md="9">
                    <p className="form-control-static" />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input">First Name</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input
                      type="text"
                      id="fname"
                      name="text-input"
                      value={this.state.board.fname}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input">Last Name</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input
                      type="text"
                      id="lname"
                      name="text-input"
                      value={this.state.board.fname}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="email-input">Email</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input
                      type="text"
                      id="email"
                      name="text-input"
                      value={this.state.board.email}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input">Subject</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input
                      type="text"
                      id="subject"
                      name="text-input"
                      value={this.state.board.subject}
                    />
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input">Message</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input
                      type="text"
                      id="message"
                      name="text-input"
                      value={this.state.board.message}
                    />
                  </Col>
                </FormGroup>
              </Form>
            </CardBody>
            <CardFooter>
            <FormGroup row>
                  <Col md="2">
                    <Label htmlFor="text-input">Reply</Label>
                  </Col>
                  <Col xs="12" md="9">
                    <Input
                      type="text"
                      id="reply"
                      name="text-input"
                      onChange={this.onChange}                     
                    />
                  </Col>
                </FormGroup>
                <Button form="tex" type="submit" size="sm" color="primary">
                    <i className="fa fa-dot-circle-o" /> Submit
                  </Button>
                  <Button form="tex" type="reset" size="sm" color="danger">
                    <i className="fa fa-ban" /> Reset
                  </Button>
            </CardFooter>
            </Card>
        </div>
    </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError,
    profile:state.firebase.profile,

  }
}

export default connect(mapStateToProps)(Reply);
//export default Show;