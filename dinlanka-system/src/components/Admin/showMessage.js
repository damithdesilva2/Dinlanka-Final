import React, { Component } from 'react';
import firebase from '../../config/fbConfig.js'
import { Link } from 'react-router-dom';
import AdminNavbar from '../layouts/AdminNavbar';
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { Table ,Button, Card, CardHeader, CardFooter, CardBody,CardTitle, CardText,Row,Col} from 'reactstrap';


class Message extends Component {

  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection('comments');
    this.unsubscribe = null;
    this.state = {
      boards: []
    };
  }

  onCollectionUpdate = (querySnapshot) => {
    const boards = [];
    querySnapshot.forEach((doc) => {
      const { fname,lname,email,subject,message } = doc.data();
      boards.push({
        key: doc.id,
        doc, // DocumentSnapshot
        fname,
        lname,
        email,
        subject,
        message

      });
    });
    this.setState({
      boards
   });
  }

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }
  
  render() {
    const { auth,authError, profile } = this.props;
    if(!auth.uid && !profile.isAdmin){return <Redirect to='/signin' />}
    return (
      <React.Fragment>
      <AdminNavbar/>
      <Row></Row>
       
      <div class="container"> 
        <Row>
          <Col>
        <Card>
        <CardHeader className="bg-primary">
                <strong>
                  <h3>Messages</h3>
                </strong>
        </CardHeader>
        <CardBody>
            
          <Table >
              <thead>
                <tr>
                  <th>First Name</th>
                  <th>Last Name</th>
                  <th>Email</th>
                  <th>Subject</th>
                  <th>Message</th>
                </tr>
              </thead>
              <tbody>
              {this.state.boards.map((board, index) =>
                  <tr key={index}>
                    <td>{board.fname}</td>
                    <td>{board.lname}</td>
                    <td>{board.email}</td>
                    <td>{board.subject}</td>
                    <td>{board.message}</td>
                    <td><Button><Link to={`/replyMessage/${board.key}`}>Reply</Link></Button></td>
                  </tr>
                )}
              </tbody>
            </Table>
            </CardBody>
            </Card>
            </Col>
            </Row>
      </div>
    </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError,
    profile:state.firebase.profile,

  }
}

export default connect(mapStateToProps)(Message);

//export default Message;