import React, { Component } from 'react';
import firebase from '../../config/fbConfig.js'
import { Link } from 'react-router-dom';
import AdminNavbar from '../layouts/AdminNavbar';
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { Table ,Button, Card, CardHeader, CardFooter, CardBody,CardTitle, CardText,Row,Col,Badge} from 'reactstrap';


class Manage extends Component {

  constructor(props) {
    super(props);
    this.ref = firebase.firestore().collection('users');
    this.unsubscribe = null;
    this.state = {
      boards: []
    };
  }

  onCollectionUpdate = (querySnapshot) => {
    const boards = [];
    querySnapshot.forEach((doc) => {
      if(doc.data().isAdmin =="no"){
      const { fullname,username,email,password,working,country,mobile } = doc.data();
      boards.push({
        key: doc.id,
        doc, // DocumentSnapshot
        fullname,
        username,
        password,
        working,
        country,
        email
      });
    }});
    this.setState({
      boards
   });
  }

  componentDidMount() {
    this.unsubscribe = this.ref.onSnapshot(this.onCollectionUpdate);
  }
  
  render() {
    const { auth,authError, profile } = this.props;
    if(!auth.uid && !profile.isAdmin){return <Redirect to='/signin' />}
    return (
      <React.Fragment>
      <AdminNavbar/>
      <Row></Row>
       
      <div className="container"> 
        <Row>
          <Col>
        <Card>
        <CardHeader className="bg-primary">
                <strong>
                  <h3>Agent Details</h3>
                </strong>
        </CardHeader>
        <CardBody>
            
          <Table>
              <thead>
                <tr>
                  <th>Full Name</th>
                  <th>User Name</th>
                  <th>Email</th>
                </tr>
              </thead>
              <tbody>
              {this.state.boards.map((board, index) =>
                  <tr key={index}>
                    <td><Link to={`/show/${board.key}`}>{board.fullname}</Link></td>
                    <td>{board.username}</td>
                    <td>{board.email}</td>
                  </tr>
                )}
              </tbody>
            </Table>
            </CardBody>
            </Card>
            </Col>
            </Row>
      </div>
    </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    auth: state.firebase.auth,
    authError: state.auth.authError,
    profile:state.firebase.profile,

  }
}

export default connect(mapStateToProps)(Manage);

//export default Manage;