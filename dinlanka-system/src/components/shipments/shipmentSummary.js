
import React , { Component }from 'react'
import { Button } from 'reactstrap';

 

const ShipmentSummary = ({project, getinfo}) => {
  
 
  const id = project.shipment_id;
  return (
      <React.Fragment>
   
    <div className="card border-primary  mb-3">
       
        <div className="card-body text-black">
        <div className="row">
          <div className="col-12">
          <h5 className="card-title">Shipment Id - {project.shipment_id}</h5>
          <p className="card-text">Customer Name - {project.customer} <br/> Origin Port - {project.origin_port} <br/> 
          Destination Port - {project.desti_port} <br/>
          Shipment Type - {project.type}<br/>
          Shipped Date - {project.ship_date}</p>
        
          <div className="row">
          <div className="col-md-6">
        
          </div>
          <div className="col-md-6 text-right">
          <Button  size="sm" color="success"><i className="fa fa-dot-circle-o"></i> Edit</Button>
                <Button form="tex" type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Delete</Button>
                <button color="danger" onClick={(e) => { if (window.confirm('Are you sure you wish to delete this item?')) getinfo(project.shipment_id) } }>
              Delete
</button>
               
          </div>
          </div>
</div>
</div>
        </div>
      </div>
  </React.Fragment>
  )
}


export default ShipmentSummary