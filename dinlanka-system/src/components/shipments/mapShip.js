import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import {Link} from 'react-router-dom'
import AgentNavbar from '../layouts/AgentNavbar';
import {Card,CardHeader} from 'reactstrap';

var firebase = require('firebase')

class  MapShip  extends Component{
constructor(props){
  super(props)
  this.state={
    Data:[],
    modal: false
  }
}

componentDidMount(){
var ref =  firebase.firestore().collection('shipments')
ref.get().then(snapshot => {
var Data=[]
snapshot.forEach(data=>{
 console.log(data.data())
Data.push(data.data())
})
this.setState({Data:Data})
})
}


  static defaultProps = {
    center: {
      lat: 24.6050353
      ,
      lng: 32.7470408
    },
    zoom: 4
  };
  
  renderMarkers(map, maps) {
    this.state.Data.map(data=>{
      var infowindow = new maps.InfoWindow({
        maxWidth: 300,
        content: '<div id = \"infowindow\"><strong>' + data.shipment_id + '</strong><br>' +
         
        data.customer + '</div>'
      });
      let marker = new maps.Marker({
        
        position:{lat: data.location2._lat, lng: data.location2._long} ,
        animation: maps.Animation.DROP,
        map:map,
        title: data.shipment_id
      });
      marker.addListener('click', function() {
        infowindow.open(map, marker);
        marker.setAnimation(maps.Animation.BOUNCE);
        setTimeout(function () {
          marker.setAnimation(null);
      }, 4000)
      });
    })
  }

  render(){
     
    return (
        <React.Fragment>
        <AgentNavbar/>

        
      <div style={{ height: '100vh', width: '80%', padding: '50px', margin:'auto', border:'5px', borderColor:'#f89d13' }}>
      <div style={{paddingBottom:'60px'}}>
     
              </div>
  {this.state.Data.length>0 ?
  
    

<div style={{ height: '100vh', width: '80%', padding: '50px', margin:'auto', border:'5px', borderColor:'#f89d13' }}>
<div className="row">
  <div className="col-12">
    <div className="card">
    <Card>
              <CardHeader className='bg-primary' >
                <strong><h3></h3></strong> 
              </CardHeader>
              </Card>
      <div className="card-body p-0">
        <div className="row p-5">
          <div className="col-md-6">
            <img src={require('../../../public/logo_1620.jpg')} />
          </div>
          <div className="col-md-6 text-right">
            <h3 className="font-weight-bold mb-1">Ongoing Shipments</h3>
            <p className="text-muted">Dinlanka </p>
          </div>
        </div>
        <hr className="my-5" />
      
       
        
        <div className="row p-5">
          <div className="col-md-12">
          <div  style={{ height: '80vh', width: '100%'}}>
          <GoogleMapReact
bootstrapURLKeys={{ key: "AIzaSyBrNnazhg3iQlhgWfw9gT6FtTKy9pwJpVY" }}
defaultCenter={this.props.center}
defaultZoom={this.props.zoom}
yesIWantToUseGoogleMapApiInternals
onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}

>
</GoogleMapReact> 
          </div>
        </div>
        </div>
    
      </div>
    </div>
  </div>
</div>

</div>

        

        
        :
        <div>please wait..........</div>
  }
   
    </div>
    </React.Fragment>
    )
    }
}

export default MapShip