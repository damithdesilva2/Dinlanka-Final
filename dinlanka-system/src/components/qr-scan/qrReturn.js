import React, { Component } from 'react'
import {  Modal, ModalHeader, ModalBody, ModalFooter, Button } from 'reactstrap';


class QrReturn extends Component {
    constructor(props){
        super(props)
    
        this.state = {
            modal: true
        }
    
        this.direct = this.direct.bind(this);
    }
    direct() {
      this.props.history.push('/');
    }
    
    toggle = () => {
      this.setState(prevState => ({
        modal: !prevState.modal
      }));
    }

    render() {
        return (
            <Modal isOpen={this.state.modal} toggle={this.toggle} className={this.props.className}>
            <ModalHeader toggle={this.toggle}>Shipment Location</ModalHeader>
            <ModalBody>
                Shipment Location Updated Successfully.
            </ModalBody>
            <ModalFooter>
            <Button color="success" onClick={this.direct}>Okay</Button>{' '}
           
            </ModalFooter>
            </Modal>
        )
    }


}


export default QrReturn;