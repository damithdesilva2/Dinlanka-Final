import React, { Component } from 'react'
import { connect } from 'react-redux'

import { Link } from 'react-router-dom'
import { withRouter } from "react-router";
import firebase from "../../config/fbConfig.js";
import GoogleMapReact from 'google-map-react';;
import Background from '../../images/cover.jpg'
import logo from '../../assets/logo-edited.jpg'


import {
  
  Button,
  Card,
  CardBody,
  CardFooter,
  CardHeader,
  Col,
  Form,
  FormGroup,
  Input,
  Label,
  Row,
  Badge

} from 'reactstrap';


class Track extends Component {
    constructor(props){
        super(props) 
        this.state = {
            reference:'',
            Data:[]
        }
    }
    getinfo = (id) => {
      console.log(id)
      const docref = firebase.firestore().collection('shipments');
      
      docref.where('shipment_id', '==', id).get().then(snapshot => {
        var Data=[]
        snapshot.forEach(data=>{
         console.log(data.data())
        Data.push(data.data())
        })
        this.setState({Data:Data})
        console.log("check");
        })
      
      }
    handleChange = (e) => {
    
        this.setState({
          [e.target.id]: e.target.value
        })
        
      }

      handleSubmit = (e) => {
        e.preventDefault();
        console.log(this.state);
        this.getinfo(this.state.reference);

      }
      static defaultProps = {
        center: {
          lat: 24.6050353
          ,
          lng: 32.7470408
        },
        zoom: 4
      };
      
      renderMarkers(map, maps) {
        this.state.Data.map(data=>{
          var infowindow = new maps.InfoWindow({
            maxWidth: 300,
            content: '<div id = \"infowindow\"><strong>' + data.shipment_id + '</strong><br>' +
             
            data.customer + '</div>'
          });

          const icon = { url: 'https://www.nicepng.com/png/full/96-967276_ship-icon-png-blue.png', scaledSize: { width: 48, height: 48 } };
          let marker = new maps.Marker({
            
            position:{lat: data.location2._lat, lng: data.location2._long} ,
            animation: maps.Animation.DROP,
            map:map,
            icon:icon,
            title: data.shipment_id
          });
          marker.addListener('click', function() {
            infowindow.open(map, marker);
            marker.setAnimation(maps.Animation.BOUNCE);
            setTimeout(function () {
              marker.setAnimation(null);
          }, 4000)
          });
        })
      }

    render() {
      
        return(
            <React.Fragment>
              <div className="bg-img" style={{backgroundImage: "url(" + Background + ")",minHeight: '100px'}}>
        <div className="row" style={{overflowX:'hidden'}}> 
        <div className="col-md-6">
        <h1 className="mb-0"><a href="/" className="text-white h2 mb-0"><img src={logo} style={{marginTop:'25px', marginLeft:'50px'}}/></a></h1>
          </div>
  <div className="col-md-6 text-right">
  <a className="btn btn-primary " style={{marginTop:'25px', marginRight:'10px'}} href="/" role="button">Go Back To Home</a>
     
      
   
  </div>
  </div>
</div>
              {this.state.Data.length<1 ?
                <div className="container"  style={{width: '900px',padding:'15px'}}>


<Card style={{padding:'15px'}} >
      <CardHeader className='bg-primary'>
        <strong>Live Shipment Tracking</strong>
        <small> Dinlanka (Pvt) Ltd</small>
      </CardHeader>
      <Form id="tex" onSubmit={this.handleSubmit} className="form-horizontal">
      <CardBody>
        <Row>
          <Col xs="6">
          <FormGroup row>
              
              <Label htmlFor="email-input">Shipment Reference</Label>
          
              <Input type="text" id="reference" name="text-input" placeholder="Shipment Reference Number" onChange={this.handleChange} />
             
       
          </FormGroup>
          
          </Col>
          </Row>
        
      </CardBody>


      <CardFooter>
        

        <Button form="tex" type="submit" size="sm" color="primary"><i className="fa fa-dot-circle-o"></i> Submit</Button>
        <div className="divider" style={{width:'30px',height:'auto',display:'inline-block'}}/>
      
        <Button form="tex" type="reset" size="sm" color="danger"><i className="fa fa-ban"></i> Reset</Button>
      </CardFooter>
      </Form>
    </Card>
    </div>
    :
   
    <div>
    <div style={{ height: '100vh', width: '80%', padding: '50px', margin:'auto', border:'5px', borderColor:'#f89d13' }}>
        <div className="row">
          <div className="col-12">
            <div className="card">
              <div className="card-body p-0">
                <div className="row p-5">
                  <div className="col-md-6">
                    <img src={require('../../../public/logo_1620.jpg')} />
                  </div>
                  <div className="col-md-6 text-right">
                    <h3 className="font-weight-bold mb-1">Shipment Live Tracking</h3>
                    <p className="text-muted">Dinlanka </p>
                  </div>
                </div>
                <hr className="my-5" />
              
                <div className="row pb-5 p-5">
                  <div className="col-md-4">
                    <p className="font-weight-bold mb-4">Shipment Information</p>
                    <p className="font-weight-bold mb-1">Shipment ID- {this.state.Data[0].shipment_id}</p>
                    <p>{this.state.Data[0].customer}</p>
                    <p className="mb-1">From - {this.state.Data[0].origin_port} </p>
                    <p className="mb-1">To - {this.state.Data[0].desti_port}</p>
                  </div>
                  <div className="col-md-4 ">
                    <p className="font-weight-bold mb-4">Last CheckPoint</p>
                    <p className="font-weight-bold mb-1">{this.state.Data[0].location}</p>
                    <p></p>
                    <p className="mb-1">Passed at:</p>
                    <p className="mb-1">{this.state.Data[0].c_time}</p>
                  </div>
                  <div className="col-md-4 ">

                  </div>
                </div>
                
                <div className="row p-5">
                  <div className="col-md-12">
                  <div  style={{ height: '80vh', width: '100%'}}>
                  <GoogleMapReact
    bootstrapURLKeys={{ key: "AIzaSyBrNnazhg3iQlhgWfw9gT6FtTKy9pwJpVY" }}
    defaultCenter={this.props.center}
    defaultZoom={this.props.zoom}
    yesIWantToUseGoogleMapApiInternals
    onGoogleApiLoaded={({map, maps}) => this.renderMarkers(map, maps)}

  >
    </GoogleMapReact> 
                  </div>
                </div>
                </div>
            
              </div>
            </div>
          </div>
        </div>
       
      </div>
    <div style={{ height: '100vh', width: '80%', padding: '50px', margin:'auto', border:'5px', borderColor:'#f89d13' }}>
    
  
  
    
    <div>Please Wait</div>

  
    
    </div>
    </div>

}
            </React.Fragment>
        )
    }
}
export default Track